import random
import os.path

total_score = 0
exit_boolean = 0
username = ''
total_Problems_Tried = 0
total_Correct_Answers = 0

# private values
addingOperator_fails = 0
substractOperator_fails = 0


"""files"""
newUser_boolean = 0



def main():

    """Stablish the loop"""
    global total_score, exit_boolean, username

    username = input("Please introduce username: ")
    checkUsername(username)

    while exit_boolean == 0:
        value_a = random.randint(0, 10)
        value_b = random.randint(0, 10)

        random_sign = random.randint(1, 3)
        StablishOperation(random_sign, value_a, value_b)



def checkUsername(name_str):

    global username

    if os.path.isfile(str(username) + ".txt"):
        print("FILE FOUND!")
        ReadWriteFile(username, "load")
    else:
        print("Creating new Save File for " + str(username))
        ReadWriteFile(username, "save")



def ReadWriteFile(filename, savingOrReading):
    global username, total_score, total_Problems_Tried, total_Correct_Answers

    if savingOrReading == "save":
        file = open(filename + ".txt", 'w')
        file.write(username + "\n")
        file.write(str(total_score) + "\n")
        file.write(str(total_Problems_Tried) + "\n")
        file.write(str(total_Correct_Answers) + "\n")
    elif savingOrReading == "load":
        file = open(filename + ".txt", 'r')
        print("Welcome " + file.readline())
        total_score = int(file.readline())
        total_Problems_Tried = int(file.readline())
        total_Correct_Answers = int(file.readline())
    else:
        """DO NUTHIN"""

    file.close()


def UserInput():
    global exit_boolean, total_score, username, total_Problems_Tried, total_Correct_Answers

    userInput = input(">>")

    if userInput == 'quit':
        exit_boolean = 1
        print("Goodbye '" + username + "' !")
        ReadWriteFile(username, "save")
        return None
    elif userInput == 'score':
        print("___________________________________________________________")
        print("USERNAME: " + username)
        print("Score is " + str(total_score))
        print("Problems tried: " + str(total_Problems_Tried))
        print("Problems answered correctly: " + str(total_Correct_Answers))
        print("Ratio: " + str(total_Correct_Answers/total_Problems_Tried))
        print("___________________________________________________________")
        return None
    else:
        return userInput



def OperationResolution(result, input):
    global total_score, total_Problems_Tried, total_Correct_Answers

    if int(input) == result:
        print("CORRECT!")
        total_score = total_score + 2
        total_Correct_Answers = total_Correct_Answers + 1
    else:
        print("WRONG! the answer is: " + str(result))
        total_score = total_score - 1

    total_Problems_Tried = total_Problems_Tried + 1



def StablishOperation(signOfOperation, a, b):

    if signOfOperation == 0:
        """Is an Adding"""
        result = a + b
        print("What is: " + str(a) + "+" + str(b) + " ?")

    elif signOfOperation == 1:
        """Is a Substract"""
        result = a - b
        print("What is: " + str(a) + "-" + str(b) + " ?")

    else:
        """Is a Dot Product"""
        result = a * b
        print("What is: " + str(a) + "*" + str(b) + " ?")


    userResponse=UserInput()
    if userResponse is not None:
        OperationResolution(result, userResponse)



main()